---
layout: comparison_page
title: GitLab compared to other tools
suppress_header: true
image_title: '/images/comparison/title_image.png'
extra_css:
  - compared.css
---

## GitLab.com vs. Bitbucket.org

### Everything mentioned in <a href='/comparison/gitlab-ce-ee-vs-bitbucket-server.html'>GitLab CE/EE versus Bitbucket Server</a>

### Unlimited collaborators
With GitLab.com, you don't have to start paying when you have more than 5 collaborators across all your private repositories.
